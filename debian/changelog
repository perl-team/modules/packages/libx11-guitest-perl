libx11-guitest-perl (0.28-3) unstable; urgency=medium

  * Team uploads.

  [ gregor herrmann ]
  * debian/copyright: replace tabs with spaces / remove trailing
    whitespace.
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Repository.
  * Update standards version to 4.1.5, no changes needed.

  [ gregor herrmann ]
  * Declare compliance with Debian Policy 4.6.1.
  * Bump debhelper-compat to 13.
  * Enable all hardening flags.
  * Annotate test-only build dependencies with <!nocheck>.
  * Fix a path in debian/copyright. Thanks to lintian.

 -- gregor herrmann <gregoa@debian.org>  Sun, 14 Aug 2022 21:37:22 +0200

libx11-guitest-perl (0.28-2) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Fabrizio Regalli from Uploaders. Thanks for your work!
  * Add Testsuite header for autopkgtest-pkg-perl.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Niko Tyni ]
  * Update to debhelper compat level 10
  * Update to Standards-Version 4.1.4
  * Declare that the package does not need (fake)root to build
  * Tune autopkgtest configuration to skip checks that require a $DISPLAY
  * Run the full test suite on build and autopkgtest. It seems possible
    that the failures seen earlier on the buildds were due to #651876.

 -- Niko Tyni <ntyni@debian.org>  Sun, 06 May 2018 10:42:47 +0300

libx11-guitest-perl (0.28-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Florian Schlichting ]
  * Import Upstream version 0.28
  * Update debian/copyright (new autotools files)
  * Drop patches, all applied upstream
  * Declare compliance with Debian Policy 3.9.5

 -- Florian Schlichting <fsfs@debian.org>  Fri, 11 Apr 2014 21:37:46 +0200

libx11-guitest-perl (0.27-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Florian Schlichting ]
  * Import Upstream version 0.27
  * Update location of docs
  * Bump Standards-Version to 3.9.4 (update to copyright-format 1.0)
  * Switch dh compatibility to level 9 to enable passing of hardening flags
  * Refresh and forward patches
  * New useless-dependency-on-libXext.patch
  * Add myself to uploaders and copyright

 -- Florian Schlichting <fsfs@debian.org>  Tue, 27 Aug 2013 22:53:00 +0200

libx11-guitest-perl (0.25-2) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * Add patch ccflags.patch: include $Config{ccflags} in CCFLAGS in
    Makefile.PL. Thanks to Boris Sukholitk for the bug report.
    (Closes: #651876)

 -- gregor herrmann <gregoa@debian.org>  Tue, 13 Dec 2011 17:53:11 +0100

libx11-guitest-perl (0.25-1) unstable; urgency=low

  * Initial Release. (Closes: #626140)

 -- Fabrizio Regalli <fabreg@fabreg.it>  Thu, 9 Jun 2011 21:19:36 +0200
